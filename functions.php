<?php

// enqueue parent styles
function ns_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'ns_enqueue_styles' );

/* Prevent WordPress Core patterns from loading */
remove_theme_support( 'core-block-patterns' );

/* Prevent a block search from offering suggestions to install community blocks */
remove_action( 'enqueue_block_editor_assets', 'wp_enqueue_editor_block_directory_assets' );
remove_action( 'enqueue_block_editor_assets', 'gutenberg_enqueue_block_editor_assets_block_directory' );


/**
 * Disable the Template Editor
 */
function example_disable_template_editor_for_posts() {
    remove_theme_support( 'block-templates' );
  }
  add_action( 'current_screen', 'example_disable_template_editor_for_posts' );