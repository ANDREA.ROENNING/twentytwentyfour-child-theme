=== Twenty Twenty-Four Child theme ===  
Contributors: Andrea Roenning  
Requires at least: 6.4  
Tested up to: 6.4  
Requires PHP: 7.0  
Stable tag: 1.0  
License: GPLv2 or later  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  

== Description ==

Twenty Twenty-Four Child Theme

# Additions

- Set lightbox on by default for image block